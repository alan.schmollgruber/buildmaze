package com.company;

import java.util.Stack;

import java.util.Random;
public class Maze {
    Random random = new Random();

    //grid variables
    int breite;
    int hoehe;
    Cell[][] grid;
    int[][] path;

    //for the path
    int currentY = 0;
    int currentX = 0;

    //last useful position
    Stack<Integer> safeY = new Stack<>();
    Stack<Integer> safeX = new Stack<>();

    //Constructor
    public Maze(int hoehe, int breite){
        this.breite = breite;
        this.hoehe = hoehe;
        initializeGrid(hoehe, breite);
        this.safeX.push(0);
        this.safeY.push(0);
    }

    //initialize Grid
    public void initializeGrid(int hoehe, int breite){
        this.grid = new Cell[hoehe][breite];
        this.path = new int[hoehe][breite];
        for (int h = 0; h < hoehe; h++) {
            for (int b = 0; b < breite ; b++) {
                this.grid[h][b] = new Cell(hoehe, breite, getHoehe(), getBreite());
                this.path[h][b] = 0;
            }
        }
    }

    //walk the path
    public void walkPath(){
        int counterSteps = 1;

        while(counterSteps < getBreite()*getHoehe()) {
            System.out.println("Step: "+counterSteps);
            this.path[getCurrentY()][getCurrentX()] = counterSteps;
            walkStep();
            counterSteps++;
        }

        if(allCellsVisited()){
            System.out.println("All the steps were visited!");
        }
    }

    //move one step
    public void walkStep(){
        boolean[] possibleDirectionsStep = possibleMoveDirections(getCurrentY(),getCurrentX());
        boolean movePossibleStep = movePossible(possibleDirectionsStep);
        if(movePossibleStep){
            int newDirection = newDirection(possibleDirectionsStep);
            updatePositionDirection(newDirection);
        } else {
            setCurrentY(getSafeY());
            setCurrentX(getSafeX());
        }
    }

    // compute the posible move directions, given the current position (height y , width x )
    public boolean[] possibleMoveDirections(int currentY, int currentX){

        //Nachbarn: in Uhrzeigersinn: [oben, rechts, unten, links]
        boolean[] unvisitedNeighbour = new boolean[]{false,false,false,false};

        //oben
        if(currentY>0 && !getGrid()[currentY-1][currentX].isVisited()){
            unvisitedNeighbour[0] = true;
        }

        //links
        if(currentX>0 && !getGrid()[currentY][currentX-1].isVisited()){
            unvisitedNeighbour[3] = true;
        }

        //rechts
        if(currentX<getBreite()-1 && !getGrid()[currentY][currentX+1].isVisited()){
            unvisitedNeighbour[1] = true;
        }

        //unten
        if(currentY<getHoehe()-1 && !getGrid()[currentY+1][currentX].isVisited()){
            unvisitedNeighbour[2] = true;
        }
        return unvisitedNeighbour;
    }

    // check if, given the posible move directions, a move is posible
    public boolean movePossible(boolean[] posibleMoveDirection){
        int degreeFreedom = 0;
        for(boolean direction:posibleMoveDirection){
            degreeFreedom += direction? 1 :0;
        }
        return ((degreeFreedom>0)? true : false) ;
    }

    //find the randomized new direction
    public int newDirection(boolean[] posibleMoveDirection){
        int newRandomDirection = 1;
        while (true){
            newRandomDirection = random.nextInt(4);
            if(posibleMoveDirection[newRandomDirection]){
                break;
            }
        }
        return newRandomDirection;
    }

    //use the direction to set the new X and Y position
    public void updatePositionDirection(int newDirection){
        this.grid[getCurrentY()][getCurrentX()].setNachbarn(newDirection,true);
        this.grid[getCurrentY()][getCurrentX()].setVisited(true);
        System.out.println("\nPosition change" +
                "\nThe old position is: ("+getCurrentY()+","+getCurrentX()+").");
        switch (newDirection){
            case 0:
                this.grid[getCurrentY()-1][getCurrentX()].setNachbarn(2,true);
                setCurrentY(getCurrentY()-1);
                break;
            case 1:
                this.grid[getCurrentY()][getCurrentX()+1].setNachbarn(3,true);
                setCurrentX(getCurrentX()+1);
                break;
            case 2:
                this.grid[getCurrentY()+1][getCurrentX()].setNachbarn(0,true);
                setCurrentY(getCurrentY()+1);
                break;
            case 3:
                this.grid[getCurrentY()][getCurrentX()-1].setNachbarn(1,true);
                setCurrentX(getCurrentX()-1);
                break;
        }
        setNewSafeY(getCurrentY());
        setNewSafeX(getCurrentX());
        System.out.println("\nThe new position is: ("+getCurrentY()+","+getCurrentX()+").");
    }

    //print the history of visit in the grid
    public void printOrderVisitPath(){
        System.out.println("\n Visit order grid:\n");
        for (int h = 0; h < getHoehe() ; h++) {
            System.out.println("  ");
            for (int b = 0; b < getBreite() ; b++) {
                System.out.print("\t"+this.path[h][b]);
            }
        }
    }


    public void printOrderVisitPathGraphic() {
        System.out.print("\nMaze Path\n");
        for (int h = 0; h < getHoehe(); h++) {
            //obere Zeile
            System.out.print("\n");
            for (int b = 0; b < getBreite(); b++) {
                String zelleOben = "";
                if (this.grid[h][b].getNachbarn()[0]) {
                    zelleOben += "    ";
                } else {
                    zelleOben += "____";
                }
                zelleOben += "";
                System.out.print("\t" + zelleOben);
            }
            System.out.print("\n");
            //mittlere Zeile
            for (int i = 0; i < 2; i++) {
                for (int b = 0; b < getBreite(); b++) {
                    String zelleMitte = "";
                    if (this.grid[h][b].getNachbarn()[3]) {
                        zelleMitte += " ";
                    } else {
                        zelleMitte += "|";
                    }
                    if(this.grid[h][b].isVisited()){
                        zelleMitte += "    ";
                    } else {
                        zelleMitte += "XXXX";
                    }

                    if (this.grid[h][b].getNachbarn()[1]) {
                        zelleMitte += " ";
                    } else {
                        zelleMitte += "|";
                    }
                    System.out.print("\t" + zelleMitte);
                }
                System.out.print("\n");
            }

            /*System.out.print("\n");*/
        }
        System.out.print("\n");

        //untere Zeile
        for (int b = 0; b < getBreite(); b++) {
            String zelleUnten = "";
            if (this.grid[getHoehe()-1][b].getNachbarn()[2]) {
                zelleUnten += "    ";
            } else {
                zelleUnten += "____";
            }
            zelleUnten += "";
            System.out.print("\t" + zelleUnten);
        }
    }

    //print the history of visit in the grid
    public void printGridVisited(){
        System.out.println("\n Visit boolean grid:\n");
        for (int h = 0; h < getHoehe() ; h++) {
            System.out.println("  ");
            for (int b = 0; b < getBreite() ; b++) {
                System.out.print(this.grid[h][b].isVisited() + " | ");
            }
        }
    }

    //check if all the cells are visited
    public boolean allCellsVisited(){
        int allCells = 0;
        for (int h = 0; h < getHoehe(); h++) {
            for (int b = 0; b < getBreite(); b++) {
                allCells += this.grid[h][b].isVisited()? 1:0;
            }
        }
        return (allCells==getBreite()*getHoehe());
    }

    //getter
    public int getBreite() {
        return breite;
    }

    public int getHoehe() {
        return hoehe;
    }

    public Cell[][] getGrid() {
        return grid;
    }

    public int getCurrentY() {
        return currentY;
    }

    public void setCurrentY(int currentY) {
        this.currentY = currentY;
    }

    public int getCurrentX() {
        return currentX;
    }

    public void setCurrentX(int currentX) {
        this.currentX = currentX;
    }

    public int getSafeY() {
        return this.safeY.pop();
    }

    public void setNewSafeY(int safeY) {
        this.safeY.push(safeY);
    }

    public int getSafeX() {
        return this.safeX.pop();
    }

    public void setNewSafeX(int safeX) {
        this.safeX.push(safeX);
    }
}