package com.company;

public class Main {

    //mazeTest
    public static void mazeTest(){
        Maze maze = new Maze(5,15);
        maze.walkPath();
        maze.printOrderVisitPath();
        maze.printOrderVisitPathGraphic();
        maze.printGridVisited();

        System.out.println();
        System.out.println("This is the third version of the program.");
    }

    public static void main(String[] args) {
        //Maze Test Example
        System.out.println("\nMaze Example:\n");
        mazeTest();


        MiniPrints print = new MiniPrints("blue",5,"sky");
        print.createsMiniPrints();

    }
}
