package com.company;

public class Cell {

    int posX ;
    int posY;
    boolean visited;

    //Nachbarn: in Uhrzeigersinn: [oben, rechts, unten, links]
    boolean[] nachbarn = {false,false,false,false};

    public Cell(int posY, int posX){
        this.posX = posX;
        this.posY = posY;
        this.visited = false;
    }

    public Cell(int posY, int posX, int breite, int hoehe){
        this.posX = posX;
        this.posY = posY;
        this.visited = false;
        //initNachbarn(breite,hoehe);
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public boolean[] getNachbarn() {
        return nachbarn;
    }

    public void setNachbarn(int nachbar, boolean besucht) {
        this.nachbarn[nachbar] = besucht;
    }

    public void initNachbarn(int hoehe, int breite){
        if (this.posY==0){
            this.nachbarn[0] = true;
        }
        if (this.posY==hoehe-1){
            this.nachbarn[2] = true;
        }
        if (this.posX==0){
            this.nachbarn[3] = true;
        }
        if (this.posX==breite-1){
            this.nachbarn[1] = true;
        }
    }


}
