package com.company;

public class Prints {
   String name;
   int size;
   String origin;

    public Prints(String name, int size, String origin) {
        this.name = name;
        this.size = size;
        this.origin = origin;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public String getOrigin() {
        return origin;
    }



    public void createAPrint(){
        System.out.println("A print");
    }
}
